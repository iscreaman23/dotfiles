" Set install plug.vim if doesn't exist
if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')

	" bclose"
	Plug 'rbgrouleff/bclose.vim'

	" francoiscabrol/ranger.vim
	Plug 'francoiscabrol/ranger.vim'

	" vim-airline
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'

	"Plug 'https://gitlab.com/rwxrob/vim-pandoc-syntax-simple'
	" vim-help
	Plug 'https://gitlab.com/iscreaman23/vimhelp' 	" personal help file config
	Plug 'jez/vim-superman'  						" vimmy and colorful man pages
	Plug 'vim-utils/vim-man'  						" vimmy and colorful man pages

	" w3m.vim
	Plug 'https://gitlab.com/iscreaman23/w3m.vim'	" personal config for using w3m within vim
	Plug 'Shougo/vimproc.vim', {'do' : 'make'}		" needed to get w3m working

	" vim-addon-mw-utils
	Plug 'marcweber/vim-addon-mw-utils' 			" don't remember what this does but stuff breaks w/o it

	" Writing
	Plug 'https://gitlab.com/iscreaman23/vimwiki'	" personal config for vimwiki
	Plug 'vim-pandoc/vim-pandoc'							" pandoc support
	Plug 'vim-pandoc/vim-pandoc-syntax'					" pandoc syntax highlighting
	Plug 'suan/vim-instant-markdown', {'for': 'markdown'} 	" realtime browser based md preview (still needs config)
	Plug 'xuhdev/vim-latex-live-preview', {'for': 'tex'} 	" realtime pdf preview of latex
	"Plug 'lervag/vimtex'

	" Most Recent File
	Plug 'yegappan/mru'					" saves most recent vim files

	" Reading
	Plug 'junegunn/goyo.vim'			" readability mode

	" Navigation/Keyboard
	Plug 'easymotion/vim-easymotion' 	" letter driven jumping
	Plug 'LeonB/vim-previous-buffer' 	" adds sensible :previousBuffer
	Plug 'liuchengxu/vim-which-key'  	" leader key hints

	" Tmux
	Plug 'tmux-plugins/vim-tmux'			" niceties for editing .tmux.conf
	Plug 'christoomey/vim-tmux-navigator'	" get tmux and vim to play together

	" Tagbar (ctags)
	Plug 'majutsushi/tagbar'			" Bar to list tags

	" tpope: The True UNIX Chad
	Plug 'tpope/vim-tbone' 				" Tmux support
	Plug 'tpope/vim-unimpaired'			" Extra bracket motions
	Plug 'tpope/vim-fugitive'			" Ex commands for git
	Plug 'tpope/vim-surround'			" change surrounding parenthesis
	Plug 'tpope/vim-repeat'				" Improves '.' for repeating motions from t-pope plugins
	Plug 'jlzhjp/vim-pair'				" Create pairs of brackets and quotes

	" Toggled Utilities
	Plug 'scrooloose/nerdtree'			" navigation

	" Shell Scripting
	Plug 'itspriddle/vim-shellcheck'	" real time shell scripting hints/suggestions

	" Style and Corrections
	" Plug 'vim-syntastic/syntastic'		" real time error highlighting
	Plug 'dense-analysis/ale' 			" Check syntax in Vim asynchronously

	" Themes
	Plug 'https://gitlab.com/iscreaman23/vim-tomorrow-night-theme.git' "
	Plug 'habamax/vim-colors-defnoche'
	Plug 'joshdick/onedark.vim'
	Plug 'tomasiser/vim-code-dark'
	Plug 'drewtempelmeyer/palenight.vim'

	" ===Languages===
	" Python
	Plug 'davidhalter/jedi-vim'			" makes python in vim fun
	" Plug 'neoclide/coc-python' needs to be installed with :CocInstall coc-rls

	" Rust
	Plug 'arzg/vim-rust-syntax-ext'		" Additional rust syntax highlighting
	Plug 'rust-lang/rust.vim'			" Official rust plugin
	" Plug 'neoclide/coc-rls/' needs to be installed with :CocInstall coc-rls
	" https://github.com/rust-lang/rustfmt "
	" https://github.com/rust-lang/rls "
	" https://github.com/neoclide/coc-sources

	" Bash
	"Plug 'vim-scripts/bash-support.vim'

	" Ion Syntax
	Plug 'vmchale/ion-vim'		" Syntax highlighting for ion



	" CSS
	Plug 'ap/vim-css-color'		" show hex colors in vim
	Plug 'chrisbra/Colorizer'	" show hex colors in vim

	" sxhkd-vim
	Plug 'kovetskiy/sxhkd-vim'	" sxhkd syntax highlighting

	" Git
	Plug 'airblade/vim-gitgutter'  " Slows down moving left w/ space and h
	Plug 'jreybert/vimagit'		   " Toggled interactive git

	" Auto Complete
	Plug 'neoclide/coc.nvim', {'branch': 'release'} " Serious stuff
 	"Plug 'ycm-core/YouCompleteMe'  " This is a long read/long/slows startup

	" Snippets
	Plug 'garbas/vim-snipmate'
	Plug 'SirVer/ultisnips'
	Plug 'honza/vim-snippets'
	"Plug 'sheerun/vim-polyglot'

	" Search
 	Plug 'mileszs/ack.vim' 		" Search for code in files
	Plug 'junegunn/fzf.vim'		" fuzzy search for fun things

call plug#end()
