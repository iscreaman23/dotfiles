" Source other two files
	so $NVIMPATH/plugin.vim
	so $NVIMPATH/plug_config.vim
	so $NVIMPATH/autocmd.vim

" General Settings
	set autochdir
	set clipboard+=unnamedplus
	set encoding=utf-8
	set hidden
	set mouse=a
	set nocompatible
	set number relativenumber
	set showmatch
	set splitbelow splitright

" Tabs and indentation
	set autoindent
	set copyindent
	set shiftround
	set shiftwidth=4
	set smartindent
	set smarttab
	set tabstop=4
	set shell=/bin/bash

" Text formating
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
	autocmd BufWritePre * %s/\s\+$//e
	set nowrap
	set linebreak
	filetype plugin indent on

" Syntax
	syntax on
	syntax enable

" Bottom UI
	set noshowmode
	set noruler
	set noshowcmd
	set laststatus=0
	set cmdheight=1
	set wildmenu

"https://github.com/liuchengxu/vim-which-key
" https://github.com/hecal3/vim-leader-guide
" Disable backing up/vim version control
	set nobackup
	set nowb
	set noswapfile

" Reuse old undos
	try
		set undodir=~/.vim_runtime/temp_dirs/undodir
		set undofile
	catch
	endtry

" Set to auto read when a file is changed from the outside
	set autoread
	autocmd FocusGained,BufEnter * checktime

" Searching (Disable highlight search after enter is pressed)
	set ignorecase
	set incsearch
	set smartcase
	set showmatch
	set nohls

" Remapping some vim defaults
	nnoremap Y y$
	nnoremap j gj
	nnoremap J <c-i>
	nnoremap K <c-o>
	" nnoremap ; :


" Set leader as <Space> and set WhichKey
	" set notimeout
	let g:mapleader = " "
	let g:maplocalleader = " "
	nnoremap <silent> <leader>      :WhichKey '<Space>'<CR>
	nnoremap <silent> <localleader> :WhichKey  '<Space>'<CR>
"	let g:which_key_map

	let g:which_key_map = {}

	let g:which_key_map.b = 'which_key_ignore'
	let g:which_key_map.Up = 'which_key_ignore'
	let g:which_key_map.Right = 'which_key_ignore'
	let g:which_key_map.Left = 'which_key_ignore'
	let g:which_key_map.Down = 'which_key_ignore'

	let g:which_key_map.SUp = 'which_key_ignore'
	let g:which_key_map.SRight = 'which_key_ignore'
	let g:which_key_map.SLeft = 'which_key_ignore'
	let g:which_key_map.SDown = 'which_key_ignore'

	nnoremap <silent> Q <Nop>
	nnoremap <silent> q/ <Nop>
	nnoremap <silent> q: <Nop>

" Buffer Nav
	nnoremap <silent> <leader>bb :Buffers <cr>
	nnoremap <silent> <c-b> :Buffers <cr>
 	nnoremap <silent> <c-n> :bnext  <cr>
 	nnoremap <silent> <c-p> :bprevious  <cr>

 	nnoremap <silent> <Leader>bn :bnext <cr>
 	nnoremap <silent> <Leader>bp :bprevious <cr>
 	nnoremap <silent> <Leader>bd :bdelete <cr>

	" nnoremap <silent> <localleader> :<c-u>WhichKey ']'<CR>
	" nnoremap <silent> <localleader> :<c-u>WhichKey '['<CR>
	" nnoremap <silent> <localleader> :<c-u>WhichKey 'g'<CR>
	" nnoremap <silent> <localleader> :<c-u>WhichKey 'c'<CR>
	set timeoutlen=500

" quit,write,
	" nmap <leader>q :q <cr>
	" nmap <leader>w :w! <cr>

" make completion navigation in command line use arrow keys
	cnoremap <Up> 	 <c-p>
	cnoremap <Down>  <c-n>

" Goyo plugin makes text more readable when writing prose:
"	map <leader>f :Goyo \| set bg=dark \| set linebreak<CR>

" Visual mode search
	vnoremap <silent> / :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
	vnoremap <silent> ? :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

" Nerd tree
	map <silent> <leader>n :NERDTreeToggle<CR>
	map <silent> <leader>r :Ranger<CR>
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" File nav
	nnoremap <silent> <Space>fr :History <cr>
	"nnoremap <silent> <leader>fr :History <cr>
	nnoremap <silent> <leader>ff :Files <cr>

" vim-repeat I don't know what this does though?
"	meant to use . with other tpope stuff
	silent! call repeat#set("g>vim-repeat", v:count)

" theme
	colorscheme Tomorrow-Night-Bright
	hi Normal guibg=NONE ctermbg=NONE
