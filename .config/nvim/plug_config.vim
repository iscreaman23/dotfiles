" Rust
"
  let g:vimwiki_list = [{'path': '~/docsvimwiki/',
                       \ 'syntax': 'markdown', 'ext': '.md'}]

" Ranger
	let g:ranger_map_keys=0

" 	g:ale_linters={'rust': ['rls']}
	inoremap <silent><expr> <TAB>
	      \ pumvisible() ? "\<C-n>" :
	      \ <SID>check_back_space() ? "\<TAB>" :
	      \ coc#refresh()
	inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

	function! s:check_back_space() abort
	  let col = col('.') - 1
	  return !col || getline('.')[col - 1]  =~# '\s'
	endfunction

	inoremap <silent><expr> <c-space> coc#refresh()

" vim-instant-markdown
	let g:instant_markdown_autostart = 0
	let g:instant_markdown_browser = "surf"

	let MRU_file="$HOME/.config/nvim/fileHistory"

" Gitgutter
" disable gitgutter keys
	let g:gitgutter_map_keys = 0

" add ag/the_silver_searcher to ack
	let g:ackprg = 'ag --nogroup --nocolor --column'

" fzf https://github.com/junegunn/fzf.vim
"" [Buffers] Jump to the existing window if possible
	let g:fzf_buffers_jump = 1
"
"" [[B]Commits] Customize the options used by 'git log':
"	let g:fzf_commits_log_options = '--graph --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr"'
"
"" [Tags] Command to generate tags file
"	let g:fzf_tags_command = 'ctags -R'
"
"" [Commands] --expect expression for directly executing the command
"	let g:fzf_commands_expect = 'alt-enter,ctrl-x'

" vim-pair
	let g:pair_toggle_key = "<Leader>pt"
	let g:pair_enable_cr_mapping = v:true
	let g:pair_enable_bs_mapping = v:true
	let g:pair_enable_space_mapping = v:true
	let g:pair_enable_fly_key_mapping = v:true
	let g:pair_enable_toggle_key_mapping = v:true
	let g:pair_enable_visual_mode_mapping = v:true
	let g:pair_enable_fly_key_mapping = v:false

" easy motion
	nmap s <Plug>(easymotion-s)

" firenvim
	let g:firenvim_config = {
	    \ 'globalSettings': {
	        \ 'alt': 'all',
	    \  },
	    \ 'localSettings': {
	        \ '.*': {
	            \ 'cmdline': 'neovim',
	            \ 'priority': 0,
	            \ 'selector': 'textarea',
	            \ 'takeover': 'always',
	        \ },
	    \ }
	\ }
	let fc = g:firenvim_config['localSettings']
	let fc['.*'] = { 'takeover': 'nonempty' }

" vim-latex-live-preview
	let g:livepreview_previewer = 'zathura'

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
	let g:UltiSnipsExpandTrigger="<S-tab>"
	let g:UltiSnipsJumpForwardTrigger="<c-b>"
	let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
	let g:UltiSnipsEditSplit="vertical"

" tmux-navigator
	let g:tmux_navigator_no_mappings = 1
	nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
	nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
	nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
	nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
	"nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>

" airline
	let g:airline_section_a = ''
	let g:airline_section_c = '%t'
	let g:airline_section_x = '%y' "[1:-1]
	let g:airline_disable_status_line = 1
	let g:airline_theme = 'minimalist'
	" let g:airline#extensions#tabline#enabled = 1
	let g:airline#extensions#whitespace#enabled = 0

" vim-syntastic
"	set statusline+=%#warningmsg#
"	set statusline+=%{SyntasticStatuslineFlag()}
"	set statusline+=%*
"
"	let g:syntastic_always_populate_loc_list = 1
"	let g:syntastic_auto_loc_list = 1
"	let g:syntastic_check_on_open = 1
"	let g:syntastic_check_on_wq = 0

