" Terminal buffer
	au TermOpen * setlocal nonumber norelativenumber

" Update binds when sxhkdrc is updated.
	autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Restart polybar when updated.
	autocmd BufWritePost *polybar/config silent! !polybar -rqs $HOST</dev/null &>/dev/null & disown %1

" Update .profile
	autocmd BufWritePost .profile  !source $HOME/.profile

" Update .profile
	autocmd BufEnter,FileType *.sh :colorscheme defnoche

" Fix shitty bindings :Wut
	autocmd VimEnter,BufNewFile,BufReadPost * silent! :nnoremap <leader><leader> <Nop> \

