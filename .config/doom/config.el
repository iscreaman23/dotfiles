;;(defun run-main-py(filename)
;;  "Runs the python file in the given directory"
;;  (interactive)
;;  (shell-command (concat "python" filename)))

;;buffer-file-name
;;(python-pytest-file FILE &optional ARGS)

(defun a-test-save-hook()
  "Test of save hook"
  (when (equal major-mode 'org-mode)
    (message "hello")))

(add-hook 'after-save-hook 'a-test-save-hook)

(setq user-full-name "iScreaMan23"
      user-mail-address "scream@theScreamTeam.xyz")

(setq doom-theme 'doom-tomorrow-night)
(setq doom-font (font-spec :family "monospace" :size 20))

(setq org-directory "~/.config/doom/org")

(setq display-line-numbers-type 'relative)
(menu-bar-mode -1)

(map! :leader "g" 'magit)
