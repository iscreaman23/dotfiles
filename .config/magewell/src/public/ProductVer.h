#pragma once

#define VER_MAJOR						1
#define VER_MINOR						3
#define VER_RELEASE						0
#define VER_BUILD						4160
#define VER_DATE						"2020-01-10 16:45:59"
#define VER_CLASS		  "RC"
