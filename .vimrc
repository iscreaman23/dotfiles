if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
	Plug 'junegunn/goyo.vim'
	Plug 'vim-utils/vim-man'
	Plug 'jez/vim-superman'
	Plug 'yuratomo/w3m.vim'
	Plug 'vim-scripts/browser.vim'
	Plug 'habamax/vim-colors-defnoche'
	Plug 'tpope/vim-unimpaired'
	Plug 'tpope/vim-surround'
	Plug 'tpope/vim-repeat'
	Plug 'jlzhjp/vim-pair'
call plug#end()

set noshowmode
set nocompatible
set clipboard+=unnamedplus
set nobackup
set nowb
set noswapfile
colorscheme defnoche

let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

let g:w3m#command='/usr/bin/w3m'

set nohls

" search highlight until enter pressed second time
	set hls
	nnoremap <silent><CR> :noh<CR><CR>

set number relativenumber

filetype plugin indent on
syntax on
syntax enable
set tabstop=4
set shiftwidth=4
set encoding=utf-8
set autoindent
set smartindent
set copyindent

" Goyo plugin makes text more readable when writing prose:
	map <leader>f :Goyo \| set bg=dark \| set linebreak<CR>

" Set leader as <SPACE>
	nnoremap <SPACE> <Nop>
	let mapleader="\<Space>"
	let maplocalleader="\<Space>"
 	nmap <silent> <Leader><Leader> :bnext <cr>

