#!/bin/zsh

source ~/.profile
##External tools installed

export ZSH=~/.config/zsh

[[ ! -d $ZSH ]] && mkdir $ZSH $ZSH/cache

PROMPT=""

if [[ -z $(command -v pacman) ]]; then
	echo "BTW you should use Arch..."
	echo "No zsh config 4 you."
	sleep 3
	exit
fi

if [[ -z $(command -v cargo) ]]; then
	sudo pacman -S rust
fi

## Prompt
# https://github.com/xcambar/purs
if [[ -z $(command -v purs) ]]; then
	git clone https://gitlab.com/iscreaman23/purs.git
	cd purs
	cargo build --release
	cp target/release/purs ~/.cargo/bin/
	rm -rf ~/purs
fi

if [[ ! -f ~/.zpm/zpm.zsh ]]; then
  git clone https://github.com/zpm-zsh/zpm ~/.zpm
fi
source ~/.zpm/zpm.zsh

# zsh-users/zsh-completions,apply:fpath,fpath:/src \

zpm                             \
  zpm-zsh/background \
  iscreaman23/zsh-core-config,type:gitlab			\
  zpm-zsh/colorize,async				\
  zdharma/fast-syntax-highlighting,async           \
  softmoth/zsh-vim-mode,async					   \
  zsh-users/zsh-history-substring-search,source:zsh-history-substring-search.zsh,async \
  horosgrisa/zsh-autosuggestions,source:zsh-autosuggestions.zsh,async                  \
  lukechilds/zsh-better-npm-completion,async                                           \
  tj/git-extras,source:/etc/git-extras-completion.zsh,async                            \
  zpm-zsh/dircolors-material,async                 \

zpm \
  kutsan/zsh-system-clipboard,async 	\
  hlissner/zsh-autopair,async                      \

zpm \
  omz/taskwarrior,async \
  omz/pip,async               \
  omz/npm,async               \
  omz/command-not-found,async \
