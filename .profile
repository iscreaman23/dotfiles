bash "$HOME/bin/bash/export_path" 2>/dev/null

. ~/.profile_extras
. ~/.func.sh

export GIT_TERMINAL_PROMPT=1
export QT_QPA_PLATFORMTHEME="qt5ct"
export IPYTHONDIR="$HOME/.config/ipython"

export PATH="$PATH:$HOME/.cargo/bin:$HOME/.local/lib/go/bin:$HOME/.local/bin:$HOME/.emacs.d/bin:"

export BASH_ENV="$HOME/.bash_env"

export NVIMPATH="$HOME/.config/nvim"
export GOPATH="$HOME/.local/lib/go"

export EDITOR="nvim"
export TERMINAL="alacritty"
export READER="zathura"
export BROWSER="chromium"

alias gitdot='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias pandora='sound_start pianobar'
alias tm='tmux_switch_session'
alias m='ncmpcpp'

alias popup='alacritty_popup'
alias vi='nvim'
alias yt='mpsyt'

export EXA_COLORS='ln=36'
alias exa_color_dirs_first='/usr/bin/exa --color=always --group-directories-first'
alias ls='exa_color_dirs_first --git-ignore'
alias la='exa_color_dirs_first -a --git-ignore'
alias laa='exa_color_dirs_first -a '
alias ll='exa_color_dirs_first -l'
alias lt='exa_color_dirs_first -aT'
alias grep="grep --color=auto"
alias diff="diff --color=auto"

alias o="xdg-open"
alias r="source ~/.local/bin/ranger"
alias fish="asciiquarium"

alias t='task'
alias td='task done'
alias ta='task add'
alias ts='task start'
alias to='taskopen'

alias yt-dl="youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' "

alias grep='grep --color=auto'
alias diff='diff --color=auto'
export QT_STYLE_OVERRIDE=Adwaita-Dark

alias fe='less ~/bin/bash/fun_examples.sh'
export PATH="$PATH:/home/iscr/bin/:/home/iscr/bin/av/:/home/iscr/bin/bash/:/home/iscr/bin/browser/:/home/iscr/bin/bspwm/:/home/iscr/bin/gif/:/home/iscr/bin/note/:/home/iscr/bin/polybar/:/home/iscr/bin/popup/:/home/iscr/bin/tmux/:/home/iscr/bin/ui/:/home/iscr/bin/youtube-dl/"
