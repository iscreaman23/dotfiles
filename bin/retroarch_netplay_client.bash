#~/bin/bash

retroarch ~/games/Mario_Kart_64-USA-.z64 \
		  --verbose \
		  --libretro /usr/lib/libretro/parallel_n64_libretro.so \
		  --config ~/.config/retroarch/retroarch.cfg \
		  --c \
		  --port 55435 \
		  --nick stealthTank \
