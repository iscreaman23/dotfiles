#~/bin/bash

retroarch ~/games/'Donkey Kong Country (USA) (Rev 2).sfc' \
		  --libretro /usr/lib/libretro/bsnes2014_balanced_libretro.so \
		  --host \
		  --port 55435 \
		  --nick iscreaman
