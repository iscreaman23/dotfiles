#~/bin/bash

retroarch ~/games/Mario\ Kart\ 64\ \(USA\).z64 \
		  --libretro /usr/lib/libretro/mupen64plus_next_libretro.so \
		  --host \
		  --port 55435 \
		  --nick iscreaman
