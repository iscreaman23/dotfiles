#!/bin/bash

taskID=$(task _uuids start:today)
if [[ -z  $taskID ]]; then
	echo ""
	exit
fi

task=$(sed -n "/$taskID/s/description:[\"]\([^\"]\)+[\"]/\1/p" ~/.task/pending.data)
startTime=$(grep "$taskID" ~/.task/pending.data | grep -Eo '["][^"]+["]' | cut -d '"' -f2 | sed -n 4p)
currentTime=$(date +%s)

spentTime=$((currentTime-startTime))

hours=$((spentTime/3600))
minutes=$(((spentTime-hours*3600)/60))
seconds=$(((spentTime-hours*3600-minutes*60)))

if ((seconds<10)); then
	seconds="0$seconds"
fi

if ((minutes<10 & hours>0)); then
	minutes="0$minutes"
fi

if ((minutes<1)); then
	minutes=""
else
	minutes="$minutes:"
fi

if ((hours<1)); then
	hours=""
else
	hours="$hours:"
fi

echo "⏰ $hours$minutes$seconds  $task"
