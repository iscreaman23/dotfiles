#!/bin/bash

[[ -f ~/.profile ]] && source ~/.profile

[[ -f ~/.bash_env ]] && source ~/.bash_env

set -o vi

export INPUTRC=~/.inputrc
